library(deSolve)

FT_per_leaf = function(params){
	SD_FT = params$SD_FT
	LD_FT_slope = params$LD_FT_slope
	Dayl = params$Dayl
	LD_intercept = params$LD_intercept
	FT = rep(NA,length(Dayl))
	i = Dayl < LD_intercept
	FT[i] = SD_FT*Dayl[i]
	FT[!i] = (SD_FT * Dayl + 1/2*(Dayl - LD_intercept)^2*LD_FT_slope)[!i]
	return(FT)
}


FT_per_leaf = function(params){
	SD_FT = params$SD_FT
	LD_FT_slope = params$LD_FT_slope
	Dayl = params$Dayl
	LD_intercept = params$LD_intercept
	FT = rep(NA,length(Dayl))
	i = Dayl < LD_intercept
	FT[i] = SD_FT
	FT[!i] = SD_FT+(Dayl[!i] - LD_intercept)*LD_FT_slope
	return(FT)
}

# FT_expression = function(t,state,params){
# 	FT = state[[1]]
# 	SD_FT = params$SD_FT
# 	LD_FT_slope = params$LD_FT_slope
# 	Dayl = params$Dayl
# 	LD_intercept = params$LD_intercept
# 	if(t > Dayl) return(list(-FT,-FT))
# 	if(t < LD_intercept) return(list(0,0))
# 	return(list(LD_FT_slope,LD_FT_slope))	
# }

# Dayl = seq(8,16,by=.1)
# FT = ode(c(params$SD_FT),Dayl,FT_expression,params)
# plot(cumsum(FT[,2])*.1,sapply(Dayl,function(x) {params$Dayl = x;FT_per_leaf(params)}))


calc_leafRate = function(t,state,params){
	LN        = state[[1]]
	r         = params$rate
	LN_max    = params$LN_max
	base_rate = params$base_rate
	temp      = params$temp
	dayl      = params$Dayl
	ptus      = temp/20*dayl/(8+dayl)
	rate      = pmax(0,ptus * (base_rate + pmax(0,r*LN*(1-LN/LN_max))))
		# r * ptus * max(0,base_rate + LN *(1-LN/LN_max)))
	return(list(rate,rate))
}

params = list()
params$rate = .03
params$LN_max = 200
params$base_rate = 1.1
params$temp = 20
params$Dayl = 16
times = seq(0,200)
leaves = ode(c(2),times,calc_leafRate,params)

plot(times,leaves[,2],type='l')

params$SD_FT = 1
params$LD_FT_slope = .5
params$LD_intercept = 8


Dayl = seq(8,24,length=100)

FT_mat = matrix(0,nr = length(times),nc = length(Dayl))
Leaves_mat = matrix(0,nr = length(times),nc = length(Dayl))
for(i in 1:length(Dayl)){
	params$Dayl = Dayl[i]
	leaves = ode(c(2),times,calc_leafRate,params)
	Leaves_mat[,i] = leaves[,2]
	FT_mat[,i] = FT_per_leaf(params)*leaves[,2]
}

filled.contour(Dayl,times,t(Leaves_mat),xaxs='r')

max_FT = 1750
FT_mat[FT_mat>max_FT] = max_FT
filled.contour(Dayl,times,t(FT_mat))




FT_slope = seq(0,.5,length=100)
FT_thresh = 200

FT_mat = matrix(0,nr = length(FT_slope),nc = length(Dayl))
for(i in 1:length(Dayl)){
	params$Dayl = Dayl[i]
	leaves = ode(c(2),times,calc_leafRate,params)
	for(j in 1:length(FT_slope)){
		params$LD_FT_slope = FT_slope[j]
		FT_conc = FT_per_leaf(params)*leaves[,2]
		FT_mat[j,i] = times[which(FT_conc > FT_thresh)][1]		
	}
}
filled.contour(FT_slope,Dayl,FT_mat)
# image(times,Dayl,FT_mat,xaxs='r')



Z = FT_mat#/max(FT_mat)
cc <- colorRamp(rev(rainbow(10)))
Zsc <- (Z-min(Z))/diff(range(Z))
rgbvec2col <- function(x) do.call(rgb,c(as.list(x),list(max=255)))
colvec <- apply(cc(Zsc),1,rgbvec2col)

library(rgl)
persp3d(FT_slope,Dayl,FT_mat,col=colvec)
axes3d()


# X=seq(0,24)
# plot(X,FT_per_leaf(5,4,X,8))


# dLeaf = function(leaves,base)